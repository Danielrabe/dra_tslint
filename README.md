How to:

* install tslint

* copy this package to %userprofile% / .atom / packages

* fix hardcorded tslint config path in %userprofile% / .atom / packages / dra-tslint / lib / dra-tslint.coffee

* open typescript file in atom

* rightclick in textarea

* click "Run ts-lint"

.... if errors were found an alert-box will open

have fun


ToDo: 

1. settings for config-path and temp-path