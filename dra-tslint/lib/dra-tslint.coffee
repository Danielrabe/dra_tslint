{CompositeDisposable} = require 'atom'
shell = require 'shell'
path = require 'path'
exec = require('child_process').exec


module.exports = UsefulContextMenu =
  subscriptions: null

  activate: (state) ->
    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    @subscriptions.add atom.commands.add 'atom-workspace', 'dra-tslint-menu:tslint': => @tslint()

    @toggle()

  deactivate: ->
    @subscriptions.dispose()

  serialize: ->

  tslint: ->
    item = atom.workspace.getActivePaneItem()
    filePath = item?.getPath?()
    app = "C:\\Windows\\System32\\cmd.exe"
    args = ""
    cmdline = "\"#{app}\ #{args}"
    cmdline = "start /W \"\" " + cmdline
    exec "tslint " + "#{filePath}" + " -c F:\\git_clone\\pcon.catalog-search\\tslint.json -o " + "#{filePath}" + ".tslint"

  toggle: ->
    atom.contextMenu.add {
      'atom-text-editor': [{type: 'separator'}]
    }
    atom.contextMenu.add {
      'atom-text-editor': [
                           {label: 'Run TS-Lint', command: 'dra-tslint-menu:tslint'}
                          ]
    }
    atom.contextMenu.add {
      'atom-text-editor': [{type: 'separator'}]
    }
